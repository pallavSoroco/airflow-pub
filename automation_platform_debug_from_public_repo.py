import random
import time

import airflow
from airflow.models import DAG
from airflow.operators.python_operator import PythonOperator


def wait(ds, **kwargs):
    time_to_wait_in_secs = random.randint(1, 3)
    time.sleep(time_to_wait_in_secs)


def fail_here(ds, **kwargs):
    time_to_wait_in_secs = random.randint(1, 3)
    time.sleep(time_to_wait_in_secs)
    raise ValueError('error while connecting to the database: Network error')


args = {
    'owner': 'Airflow',
    'start_date': airflow.utils.dates.days_ago(2),
}

dag = DAG(
    dag_id='Automation_Platform_Debug_From_Public_Repo',
    default_args=args,
    schedule_interval=None,
)


def print_context(ds, **kwargs):
    pprint(kwargs)
    print(ds)
    return 'printed context'


op_container = PythonOperator(task_id='Check-Containers',
                              provide_context=True,
                              python_callable=wait,
                              dag=dag)

op_runit = PythonOperator(task_id='Check-runit',
                          provide_context=True,
                          python_callable=wait,
                          dag=dag)

op_databases = PythonOperator(task_id='Check-Databases',
                              provide_context=True,
                              python_callable=wait,
                              dag=dag)
op_processors = PythonOperator(task_id='Check-Processors',
                               provide_context=True,
                               python_callable=wait,
                               dag=dag)

op_dashboard = PythonOperator(task_id='Check-Dashboard',
                              provide_context=True,
                              python_callable=wait,
                              dag=dag)

op_clean_up = PythonOperator(task_id='Clean-Up',
                             provide_context=True,
                             python_callable=fail_here,
                             dag=dag)

op_notify = PythonOperator(task_id='Generate-Notification',
                           provide_context=True,
                           python_callable=wait,
                           dag=dag)

op_container >> op_runit >> [op_databases, op_processors, op_dashboard] >> op_clean_up >> op_notify
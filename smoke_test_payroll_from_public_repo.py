import random
import time

import airflow
from airflow.models import DAG
from airflow.operators.python_operator import PythonOperator


def wait(ds, **kwargs):
    time_to_wait_in_secs = random.randint(1, 3)
    time.sleep(time_to_wait_in_secs)


args = {
    'owner': 'Airflow',
    'start_date': airflow.utils.dates.days_ago(2),
}

dag = DAG(
    dag_id='Smoke_test_Payroll_from_public_repo',
    default_args=args,
    schedule_interval=None,
)


def print_context(ds, **kwargs):
    pprint(kwargs)
    print(ds)
    return 'printed context'


op_pre_checks = PythonOperator(task_id='Perform-Pre-Checks',
                              provide_context=True,
                              python_callable=wait,
                              dag=dag)


op_worker_vm1 = PythonOperator(task_id='Checking-Worker-VM-1',
                              provide_context=True,
                              python_callable=wait,
                              dag=dag)
op_worker_vm2 = PythonOperator(task_id='Checking-Worker-VM-2',
                               provide_context=True,
                               python_callable=wait,
                               dag=dag)

op_banking_app = PythonOperator(task_id='Checking-Banking-App',
                              provide_context=True,
                              python_callable=wait,
                              dag=dag)

op_clean_up = PythonOperator(task_id='Clean-Up',
                             provide_context=True,
                             python_callable=wait,
                             dag=dag)

op_notify = PythonOperator(task_id='Generate-Notification',
                           provide_context=True,
                           python_callable=wait,
                           dag=dag)

op_pre_checks >> [op_worker_vm1, op_worker_vm2, op_banking_app] >> op_clean_up >> op_notify